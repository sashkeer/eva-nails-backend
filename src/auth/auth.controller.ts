import { Controller, Post, UseGuards, Request, Response, Get } from '@nestjs/common';
import * as express from 'express';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { JWT_TOKEN_KEY } from '../constants';
import { getTokenFromRequest } from '../utils/request';
import { JwtService } from '@nestjs/jwt';

@Controller('auth')
export class AuthController {

    constructor (
        private authService: AuthService,
        private jwtService: JwtService
    ) { }

    @Get('validate')
    public async validate (
        @Request() req: express.Request & { user: any },
    ) {
        const token: string = getTokenFromRequest(req);
        try {
            this.jwtService.verify(token)
        } catch (e) {
            return false;
        }
        return true;
    }

    @Post('login')
    @UseGuards(AuthGuard('local'))
    public async login (
        @Request() req: express.Request & { user: any },
        @Response() res: express.Response
    ) {
        const token = await this.authService.login(req.user);
        res.cookie(JWT_TOKEN_KEY, token.access_token, { httpOnly: true });
        return res.send(true);
    }

    @Get('logout')
    public logout (
        @Request() request: express.Request,
        @Response() response: express.Response,
    ) {
        response.clearCookie(JWT_TOKEN_KEY);
        return response.send('');
    }
}

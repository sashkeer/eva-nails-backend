import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { JWT_SECRET } from '../constants';
import { getTokenFromRequest } from '../utils/request';
import { UserService } from '../user/user.service';
import { Strategy } from 'passport-jwt';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor (
    private userService: UserService,
    ) {
    super({
      jwtFromRequest: getTokenFromRequest,
      ignoreExpiration: false,
      secretOrKey: JWT_SECRET,
    });
  }

  public async validate (payload: { sub: number; name: string }) {
    const user = await this.userService.getById(payload.sub);
    return { userId: payload.sub, name: payload.name, role: user.role };
  }
}

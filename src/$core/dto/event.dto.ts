import { IsDate, IsIn, IsNotEmpty, IsString } from 'class-validator';
import { Type } from 'class-transformer';

export class GetEventDto {
    @IsNotEmpty()
    @IsDate()
    @Type(() => Date)
    public date: Date;

    @IsNotEmpty()
    @IsString()
    @IsIn(['day', 'month', 'quarter'])
    public period: string;

}

export class PostEventDto {
    @IsNotEmpty()
    @IsDate()
    @Type(() => Date)
    date: Date;

    @IsNotEmpty()
    time: string;

    @IsString()
    @IsNotEmpty()
    name: string;

    @IsString()
    @IsNotEmpty()
    service: string;

    @IsString()
    @IsNotEmpty()
    phone: string;

}
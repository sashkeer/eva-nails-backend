import { ETime, EventEntity } from './entities/event.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ServiceEntity } from './entities/service.entity';
import { UserEntity } from './entities/user.entity';

@Injectable()
export class AppService {
    constructor(
        @InjectRepository(EventEntity) public readonly repo: Repository<EventEntity>,
        @InjectRepository(UserEntity) public readonly userRepo: Repository<UserEntity>,
        @InjectRepository(ServiceEntity) public readonly serviceRepo: Repository<ServiceEntity>,
    ) {
    }
}

import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from '../user/user.service';

@Injectable()
export class AuthService {

    constructor(
        private readonly userService: UserService,
        private readonly jwtService: JwtService,
    ) {}

    public async validateUser (username: string, password: string): Promise<any> {
        const user = await this.userService.getByNameAndPassword(username, password);
        if (user) {
            return user;
        }
        return null;
    }

    public async login(user: any) {
        const payload = { name: user.name, sub: user.id, role: user.role };
        return {
            access_token: await this.jwtService.signAsync(payload),
        };
    }
}

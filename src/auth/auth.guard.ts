import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import * as express from 'express';
import { getTokenFromRequest } from '../utils/request';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor (
        private readonly reflector: Reflector,
        private readonly jwtService: JwtService,
    ) { }

    public async canActivate(context: ExecutionContext): Promise<boolean> {
        const request: express.Request & { user: any } = context.switchToHttp().getRequest();

        const token: string = getTokenFromRequest(request);
        if (!token) {
            throw new UnauthorizedException();
        }

        const payload = await this.jwtService.verifyAsync(token);
        if (payload.sub) {
            request.user = { id: payload.sub, role: payload.role };
            return true;
        }
        throw new UnauthorizedException();
    }
}

module.exports = {
    apps : [{
        name: 'Eva nails back',
        script: 'dist/main.js',
        args: '',
        instances: 1,
        autorestart: true,
        watch: false,
        max_memory_restart: '1G',
        env: {
            NODE_ENV: 'development'
        },
        env_production: {
            NODE_ENV: 'production',
            DB_USER: 'eva',
            DB_PASSWORD: 'jon746sas',
            SLACK_TOKEN: 'xoxp-857220223062-847081465025-855022352080-4e9d463ee9df0e8b7dec67b541fc4071'
        }
    }],

    deploy : {
        production : {
            user : 'root',
            host : '178.128.205.255',
            ref  : 'origin/master',
            repo : 'git@gitlab.com:sashkeer/eva-nails-backend.git',
            path : '/var/www/eva-nails-back',
            'post-deploy' : 'npm install && npm run build && pm2 reload ecosystem.config.js --env production'
        }
    }
};

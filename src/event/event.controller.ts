import { Body, Controller, Delete, Get, Param, Patch, Post, Query, Req, UseGuards } from '@nestjs/common';
import { EventService } from './event.service';
import * as express from 'express';
import { EventStatus } from '../$core/enums/event';
import { GetEventDto, PostEventDto } from '../$core/dto/event.dto';
import { AuthGuard } from '../auth/auth.guard';
const requestIp = require('request-ip');

@Controller('event')
export class EventController {

    constructor(
        private readonly eventService: EventService,
    ) { }

    @Get()
    @UseGuards(AuthGuard)
    getEvents(@Query() { date, period }: GetEventDto) {
        return this.eventService.getEvents(date, period, {
            select: null,
            relations: ['user', 'service'],
            status: [EventStatus.Confirmed, EventStatus.Created]
        });
    }

    @Get('public')
    getPublicEvents(@Query() { date, period }: GetEventDto) {
        return this.eventService.getEvents(date, period, {
            select: ['id', 'date', 'status', 'time', 'duration'],
            relations: null,
            status: [EventStatus.Confirmed]
        });
    }

    @Post()
    makeAppointment(@Body() event: PostEventDto, @Req() req: express.Request) {
        return this.eventService.saveEvent(event, requestIp.getClientIp(req));
    }

    @Post('fill')
    fillDay(@Body('date') date: Date) {
        return this.eventService.fillDay(date);
    }

    @Patch(':id')
    @UseGuards(AuthGuard)
    confirmEvent(@Param('id') id: number, @Body('sendSms') sendSms: boolean) {
        return this.eventService.setEventStatus(id, EventStatus.Confirmed, sendSms);
    }

    @Patch(':id/finish')
    @UseGuards(AuthGuard)
    finishEvent(@Param('id') id: number) {
        return this.eventService.setEventStatus(id, EventStatus.Finished);
    }

    @Patch(':id/duration')
    @UseGuards(AuthGuard)
    updateEventDuration(@Param('id') id: number, @Body('duration') duration: number) {
        return this.eventService.setEventDuration(id, +duration);
    }

    @Patch(':id/time')
    @UseGuards(AuthGuard)
    updateEventTime(@Param('id') id: number, @Body('time') time: string) {
        return this.eventService.setEventTime(id, time);
    }

    @Delete(':id')
    @UseGuards(AuthGuard)
    declineEvent(@Param('id') id: number, @Body('status') status: EventStatus) {
        return this.eventService.setEventStatus(id, EventStatus.Declined);
    }
}

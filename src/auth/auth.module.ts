import { Global, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from './auth.service';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './local.strategy';
import { JWT_SECRET } from '../constants';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { JwtStrategy } from './jwt.strategy';
import { UserModule } from '../user/user.module';
import { AuthController } from './auth.controller';
import { UserEntity } from '../entities/user.entity';

export const RegisteredJwtModule = JwtModule.register({
    secret: JWT_SECRET,
    signOptions: { expiresIn: '3600s' },
});


@Global()
@Module({
    imports: [
        RegisteredJwtModule,
        PassportModule,
        UserModule,
    ],
    providers: [
        AuthService,
        LocalStrategy,
    ],
    controllers: [AuthController],
})
export class AuthModule {}

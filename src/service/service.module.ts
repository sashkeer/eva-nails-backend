import { Module } from '@nestjs/common';
import { ServiceService } from './service.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ServiceEntity } from '../entities/service.entity';
import { ServiceController } from './service.controller';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            ServiceEntity,
        ]),
    ],
    providers: [ServiceService],
    exports: [ServiceService],
    controllers: [ServiceController],
})
export class ServiceModule {}

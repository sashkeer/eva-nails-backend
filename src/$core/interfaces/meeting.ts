import { ETime } from '../../entities/event.entity';

export interface IMeetingEvent {
    date: Date;
    time: ETime;
    service: number[];
    name: string;
    surname: string;
    phone: string;
    vkId: number;
    facebookId: number;
}
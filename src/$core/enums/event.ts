export enum EventStatus {
    Created = 'Created',
    Confirmed = 'Confirmed',
    Declined = 'Declined',
    Finished = 'Finished',
}
import { Module } from '@nestjs/common';
import { EventController } from './event.controller';
import { EventService } from './event.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EventEntity } from '../entities/event.entity';
import { UserModule } from '../user/user.module';
import { ServiceModule } from '../service/service.module';
import { RegisteredJwtModule } from '../auth/auth.module';

@Module({
    imports: [
        RegisteredJwtModule,
        TypeOrmModule.forFeature([
            EventEntity
        ]),
        UserModule,
        ServiceModule,
    ],
    controllers: [EventController],
    providers: [EventService],
})
export class EventModule {}

import { Injectable } from '@nestjs/common';
import { UserEntity } from '../entities/user.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
const md5 = require('md5');

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(UserEntity) public readonly repo: Repository<UserEntity>,
    ) { }

    public async getByPhone(phone: string) {
        return await this.repo.findOne({
            where: {
                phone,
            },
        });
    }

    public async getById(id: number) {
        return await this.repo.findOne({
            where: {
                id,
            },
        });
    }

    public async getByNameAndPassword(username: string, password: string) {
        return await this.repo.findOne({
            where: {
                password: md5(md5(password), 'shit'),
                name: username,
            },
        });
    }
}

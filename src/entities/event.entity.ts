import {Column, CreateDateColumn, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn} from "typeorm";
import {UserEntity} from "./user.entity";
import {ServiceEntity} from "./service.entity";
import { EventStatus } from '../$core/enums/event';

export enum ETime {
    morning = 'morning',
    afternoon = 'afternoon',
    evening = 'evening',
    night = 'night',
}

@Entity()
export class EventEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: true })
    ip: string;

    @Column({
        type: 'enum',
        enum: EventStatus,
        default: EventStatus.Created,
    })
    status: EventStatus;

    @ManyToOne(() => UserEntity, { cascade: true })
    @JoinColumn()
    user: UserEntity;

    @ManyToMany(type => ServiceEntity, { cascade: true })
    @JoinTable()
    service: ServiceEntity[];

    @Column({ nullable: true })
    service_name: string;

    @Column()
    time: string;

    @Column({ default: 1.5, type: 'real' })
    duration: number;

    @Column({ nullable: true })
    description: string;

    @Column()
    date: Date;

    @Column({
        type: 'json',
        nullable: true,
    })
    message: object;

    @CreateDateColumn()
    createDate: Date;
    @UpdateDateColumn()
    updateDate: Date;
}

import {Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn} from 'typeorm';

@Entity()
export class UserEntity {
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    ip: string;
    @Column()
    name: string;
    @Column()
    phone: string;
    @Column({ default: 'user' })
    role: string;
    @Column({ nullable: true })
    surname: string;
    @Column({ nullable: true })
    password: string;
    @CreateDateColumn()
    createDate: Date;
    @UpdateDateColumn()
    updateDate: Date;
}
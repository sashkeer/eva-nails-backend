import {Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn} from "typeorm";
import {UserEntity} from "./user.entity";

@Entity()
export class ServiceEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({ nullable: true })
    description: string;

    @Column({ nullable: true })
    alias: string;

    @Column()
    price: number;

    @Column({ default: false })
    deprecated: boolean;

    @CreateDateColumn()
    createDate: Date;
    @UpdateDateColumn()
    updateDate: Date;
}
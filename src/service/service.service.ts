import { Injectable } from '@nestjs/common';
import { In, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ServiceEntity } from '../entities/service.entity';

@Injectable()
export class ServiceService {
    constructor(
        @InjectRepository(ServiceEntity) public readonly repo: Repository<ServiceEntity>,
    ) { }

    getServices() {
        return this.repo.find({
            where: {
                deprecated: false,
            },
        });
    }

    public async getById(id: number): Promise<ServiceEntity> {
        return await this.repo.findOne({
            where: {
                id,
            },
        });
    }

    public async getByIds(ids: number[]): Promise<ServiceEntity[]> {
        return await this.repo.find({
            where: {
                id: In(ids),
            },
        });
    }
}

import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {TypeOrmModule, TypeOrmModuleOptions} from '@nestjs/typeorm';
import { EventModule } from './event/event.module';
import { UserModule } from './user/user.module';
import { AuthModule, RegisteredJwtModule } from './auth/auth.module';
import { ServiceModule } from './service/service.module';
import { EventEntity } from './entities/event.entity';
import { ServiceEntity } from './entities/service.entity';
import { UserEntity } from './entities/user.entity';
import { Reflector } from '@nestjs/core';
import { defaultConfig, ENTITIES } from './$core/config/typeorm';

@Module({
  imports: [
      RegisteredJwtModule,
      AuthModule,
      TypeOrmModule.forRoot(defaultConfig),
      TypeOrmModule.forFeature(ENTITIES),
      EventModule,
      UserModule,
      ServiceModule,
  ],
  controllers: [AppController],
  providers: [
      AppService,
      Reflector,
  ],
})
export class AppModule {}

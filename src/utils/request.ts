import { JWT_TOKEN_KEY } from '../constants';

export const getTokenFromRequest = function(req: any) {
    return req && req.cookies ? req.cookies[JWT_TOKEN_KEY] : null;
};

export const getUserFromRequest = function(req: any, key: string) {
  const user = req && req.user;
  if(!user) {
      return null;
  }

  return key ? user[key] : user;
};

import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { EventEntity } from '../../entities/event.entity';
import { ServiceEntity } from '../../entities/service.entity';
import { UserEntity } from '../../entities/user.entity';

export const ENTITIES = [EventEntity, ServiceEntity, UserEntity];

export const defaultConfig = {
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    database: process.env.DB_NAME || 'eva_nails',
    schema: process.env.DB_SCHEMA || 'calendar',
    logging: false,
    synchronize: true,
    dropSchema: false,
    username: process.env.DB_USER || 'postgres',
    password: process.env.DB_PASSWORD || 'postgres',
    entities: ENTITIES,
} as TypeOrmModuleOptions;
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Between, In, Repository } from 'typeorm';
import { EventEntity } from '../entities/event.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '../entities/user.entity';
import { plainToClass } from 'class-transformer';
import { UserService } from '../user/user.service';
import { ServiceService } from '../service/service.service';
import { EventStatus } from '../$core/enums/event';
import { PostEventDto } from '../$core/dto/event.dto';
import { addDays, addMonths, format } from 'date-fns';
const { WebClient } = require('@slack/web-api');
const token = process.env.SLACK_TOKEN;
const ruLocale = require('date-fns/locale/ru');
const accountTwilioSid = 'AC784650ccca78f4e87644107b02ec7f1c';
const authTwilioToken = '8bf4b00026a28f489d3667815db54166';
const client = require('twilio')(accountTwilioSid, authTwilioToken);
const web = new WebClient(token);
const conversationId = 'CR6RMDW1M';

export const CONFIRM_USER_REQUEST = `Eva Nails Perm

Вы успешно записаны на %DATE% %TIME%
 
Номер для связи +7 (951) 950 08 38
 
Адрес г.Пермь. ул. Уинская 3а кв. 153`;

@Injectable()
export class EventService {

    private readonly MAX_EVENTS = 4;
    private readonly MAX_USERS_PER_DAY = 2;

    constructor(
        @InjectRepository(EventEntity) public readonly repo: Repository<EventEntity>,
        public readonly userService: UserService,
        public readonly serviceService: ServiceService,
    ) { }

    public async getEvents(
        date: Date,
        period?: string,
        options?: {
            select?: (keyof EventEntity)[],
            relations?: string[],
            status?: EventStatus[]
        },
    ) {
        let eventsPeriod: any;
        const formatedDate = format(date, 'yyyy-MM-dd');
        const fromDate = new Date(formatedDate);
        const toDate = new Date(formatedDate);

        if (period === 'day') {
            eventsPeriod = Between(
                format(new Date(fromDate.setHours(0, 0, 0, 0)), 'dd.MM.yyyy HH:mm:ss'),
                format(new Date(toDate.setHours(23, 59, 59, 999)), 'dd.MM.yyyy HH:mm:ss'),
            );

        } else if (period === 'month') {
            eventsPeriod = Between(
                new Date(fromDate.setDate(0)),
                new Date(toDate.setDate(31))
            );
        } else if (period === 'quarter') {
            eventsPeriod = Between(
                new Date(fromDate.setDate(0)),
                new Date(addMonths(toDate, 3).setDate(31))
            );
        }
        return this.repo.find({
            select: options.select,
            relations: options.relations,
            where: {
                status: In(options.status),
                date: eventsPeriod,
            }
        });
    }

    public async setEventStatus(id: number, status: EventStatus, sendSms?: boolean) {
        if (status === EventStatus.Confirmed && sendSms) {

            const event: EventEntity = await this.repo.findOne(id, { relations: ['user', 'service'] });

            const message = await client.messages
                .create({
                    body: CONFIRM_USER_REQUEST
                        .replace('%DATE%', format(event.date, 'dd.MM.yyyy', { locale: ruLocale }))
                        .replace('%TIME%', event.time),
                    from: '+12014853742',
                    to: '+7' + event.user.phone,
                });

            return this.repo.update(id, { status, message });
        }
        return this.repo.update(id, { status });
    }

    public async setEventDuration(id: number, duration: number) {
        return this.repo.update(id, { duration });
    }

    public async setEventTime(id: number, time: string) {
        return this.repo.update(id, { time });
    }

    public fillDay(date: Date) {

        const times: string[] = [
            '10:00',
            '13:00',
            '16:00',
            '18:00',
        ];

        const dayEvents = times.map(time => plainToClass(EventEntity, {
            user: { id: 8 },
            service: [],
            time: time,
            date: date,
            status: EventStatus.Confirmed,
        }));

        return this.repo.save(dayEvents);
    }

    public async saveEvent(event: PostEventDto, ip: string): Promise<any> {
        /* Check user */
        let user: UserEntity = await this.userService.getByPhone(event.phone);
        if (!user) {
            user = plainToClass(UserEntity, {
                ip,
                phone: event.phone,
                name: event.name,
            });
        }

        /* Check Service */
        // const service: ServiceEntity[] = await this.serviceService.getByIds(event.service);

        // if (!service) {
        //     throw new HttpException('И что мне с вами делать? Вы не выбрали услугу 😐',
        //         HttpStatus.CONFLICT);
        // }

        /*
        * Check Events
        * */
        const allEvents = await this.getDailyEvents(event.date);
        const confirmedEvents = allEvents.filter(event => event.status === EventStatus.Confirmed);
        const createdEvents = allEvents.filter(event => event.status === EventStatus.Created);

        if (confirmedEvents.length > this.MAX_EVENTS) {
            throw new HttpException('В этот день все места заняты :(',
                HttpStatus.CONFLICT);
        }

        if (confirmedEvents.some((storedEvent) => storedEvent.time === event.time)) {
            throw new HttpException('На это время кто то уже записан... Попробуйте другую дату пожалуйста (;´༎ຶД༎ຶ`)',
                HttpStatus.CONFLICT);
        }

        if (createdEvents.filter((storedEvent) => storedEvent.user.phone === event.phone).length >= this.MAX_USERS_PER_DAY) {
            throw new HttpException(
                'Подозрительно много записей на один день с вашего номера ಠ_ಠ я вас запомнила,' +
                ' а событие не записала, вдруг вы спамер... КЫШ <br> (∩｀-´)⊃━☆ﾟ.*･｡ﾟ',
                HttpStatus.CONFLICT,
            );
        }

        if (createdEvents.filter((storedEvent) => storedEvent.user.ip === ip).length >= this.MAX_USERS_PER_DAY) {
            throw new HttpException(
                'Подозрительно много записей на один день с вашего IP ಠ_ಠ я вас запомнила, а событие не записала, вдруг вы спамер (ง •̀_•́)ง',
                HttpStatus.CONFLICT,
            );
        }

        let duration = 1.5;

        switch (event.service) {
            case 'Маникюр':
                duration = 1;
                break;
            case 'Наращивание':
                duration = 2;
                break;
            case 'Коррекция':
                duration = 2;
                break;
        }

        const newEvent: EventEntity = plainToClass(EventEntity, {
            ip,
            user,
            duration,
            service: [],
            service_name: event.service,
            description: event.name,
            time: event.time,
            date: event.date,
        });

        const slackMessage = `
            Новая запись, необходимо подтвердить.
            Клиент: ${user.name}
            Время: ${event.time}
            Дата: ${format(event.date, 'dd.MM.yyyy')}
            Телефон: +7${user.phone}
            IP: ${user.ip}
            http://eva-nails.ru/admin/meetings?date=${format(event.date, 'yyyy-MM-dd')}
        `;

        const savedEvent: any = this.repo.save(newEvent);
        if (!process.env.DEVELOPMENT) {
            await web.chat.postMessage({
                channel: conversationId,
                text: slackMessage,
            });
        }

        return savedEvent;
    }

    private async getDailyEvents(date: Date) {
        const formatedDate = format(date, 'yyyy-MM-dd');
        const fromDate = new Date(formatedDate);
        const toDate = new Date(formatedDate);
        return await this.repo.find({
            relations: ['user', 'service'],
            where: {
                status: In([EventStatus.Confirmed, EventStatus.Created]),
                date: Between(
                    format(fromDate, 'dd.MM.yyyy', { locale: ruLocale }),
                    format(new Date(toDate.setHours(23, 9, 9, 999)), 'dd.MM.yyyy', { locale: ruLocale }),
                ),
            },
        });
    }
}
